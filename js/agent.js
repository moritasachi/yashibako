
var ua = navigator.userAgent;
var headNode = document.getElementsByTagName("head")[0];
var isSmartPhone = false;
if( ua.indexOf('iPhone') != -1 || ua.indexOf('iPod') != -1 || ua.indexOf('Android') != -1 && ua.indexOf('Mobile') != -1 ) {
	isSmartPhone = true;
	var viewport = document.createElement('meta');
	viewport.name = 'viewport';
	viewport.content = 'width=device-width,minimum-scale=1.0,maximum-scale=1.0';
	headNode.appendChild(viewport);
} else {
	var viewport = document.createElement('meta');
	viewport.name = 'viewport';
	viewport.content = 'width=1000';
	headNode.appendChild(viewport);
}
if(isSmartPhone) {
	var stylesheet = document.createElement('link');
	stylesheet.rel = 'stylesheet';
	stylesheet.href = 'css/sp_style.css';
	headNode.appendChild(stylesheet);
} else {
	var stylesheet = document.createElement('link');
	stylesheet.rel = 'stylesheet';
	stylesheet.href = 'css/style.css';
	headNode.appendChild(stylesheet);
}
